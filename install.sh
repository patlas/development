#!/bin/sh

DIR=/root/a4wp/a4wp-simulators/dbg_logger

if ! [[ -p $DIR/dbg_pipe ]]
then
 printf "Creating pipe: dbg_pipe\n"
 mkfifo $DIR/dbg_pipe
fi

#if ! [[ -p dbg_show ]]
#then
# printf "Creating pipe: dbg_show\n"
# mkfifo dbg_show
#fi

if [ -f $DIR/ptu_logs.log ]
then
 rm $DIR/ptu_logs.log
fi

if [ -f $DIR/pru_logs.log ]
then
 rm $DIR/pru_logs.log
fi

printf "Compilling logger.c ...\n"

hostn="$(hostname)"

if [[ $hostn == *"PTU"* ]]
then
 gcc -pthread $DIR/logger.c -O0 -D PTU -o $DIR/logger
 printf "Creating log file: ptu_logs.log\n"
 touch $DIR/ptu_logs.log
else
 gcc -pthread $DIR/logger.c -O0 -D PRU -o $DIR/logger
 printf "Creating log file: pru_logs.log\n"
 touch $DIR/pru_logs.log
fi

printf "Setting permision ...\n"
chmod 755 -R $DIR
chmod +t -R $DIR
