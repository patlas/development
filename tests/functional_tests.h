//
// Created by patlas on 4/16/19.
//

#ifndef PROJECT_UNIT_TESTS_H
#define PROJECT_UNIT_TESTS_H

int suite_success_init(void);
int suite_success_clean(void);


void test_write_one_record(void);
void test_write_whole_page(void);
void test_write_whole_memory(void);
void test_mark_page_as_corrupted(void);
void test_omit_corrupted_and_write(void);
void test_load_latest_record_at_bootup_beginning(void);

#endif //PROJECT_UNIT_TESTS_H
