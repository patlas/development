//
// Created by patlas on 4/4/19.
//

#ifndef PROJECT_NRF_LOG_H
#define PROJECT_NRF_LOG_H

#include <stdio.h>
#define NRF_LOG_ERROR(f, ...) printf("%s [%s()]\t\t\t" f, "ERROR:", __func__, ##__VA_ARGS__); fflush(stdout)
#define NRF_LOG_INFO(f, ...) printf("%s [%s()]\t\t\t" f, "INFO:", __func__, ##__VA_ARGS__); fflush(stdout)
#define NRF_LOG_DEBUG(f, ...) printf("%s [%s()]\t\t\t" f, "DEBUG:", __func__, ##__VA_ARGS__); fflush(stdout)

#endif //PROJECT_NRF_LOG_H
