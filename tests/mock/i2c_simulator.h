//
// Created by patlas on 4/16/19.
//

#ifndef PROJECT_I2C_SIMULATOR_H
#define PROJECT_I2C_SIMULATOR_H

int i2c_init(void);
int i2c_exit(void);

#endif //PROJECT_I2C_SIMULATOR_H
