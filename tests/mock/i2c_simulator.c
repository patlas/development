#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <time.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <termios.h>
#include <string.h>
#include <sys/types.h>
#include <pthread.h>
#include <errno.h>

#include "i2ccm.h"
#include "memory_trimmer.h"
#include "nrf_log.h"



static pthread_t i2c_tx_thread, i2c_rx_thread;
pthread_mutex_t rx_lock, tx_lock, i2c_access;
m_i2ccm_evt_t rx_event=-1, tx_event=-1, rx_event_previous=-1;
uint8_t event_tx_count = 0, event_rx_count = 0;

pthread_mutexattr_t mattr;

extern uint8_t eeprom_memory_map[131072];


static void *i2c(void *args) {
    NRF_LOG_DEBUG("I2C started\n");
    while (1) {

        if (!pthread_mutex_trylock(&tx_lock)) {
            if (event_tx_count) {
                NRF_LOG_DEBUG("i2c_tx event cnt: %d\n", event_tx_count);
                event_tx_count--;
                NRF_LOG_DEBUG("i2c_tx_event: decrementing evt count\n");
                memory_trimmer_rx_tx_handler(tx_event);
            }
            pthread_mutex_unlock(&tx_lock);
        }


        if (!pthread_mutex_trylock(&rx_lock)) {
            if (event_rx_count) {
                NRF_LOG_DEBUG("i2c_rx event cnt: %d\n", event_rx_count);
                event_rx_count--;
                NRF_LOG_DEBUG("i2c_rx_event: decrementing evt count\n");
                memory_trimmer_rx_tx_handler(rx_event);
            }
            pthread_mutex_unlock(&rx_lock);
        }
//        usleep(10000);
    }
}


/* Starts tx/rx thread */
static bool start_i2c(void)
{
    int er = 0;
    er = pthread_create(&i2c_tx_thread, NULL, i2c, NULL);

    if(er!= 0)
    {
        return false;
    }
  return true;
}

int i2c_exit(void) {
    // kill thread
    return pthread_cancel(i2c_tx_thread);
}

int i2c_init(void)
{
    pthread_mutexattr_init(&mattr);
    pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&rx_lock, &mattr);
    pthread_mutex_init(&tx_lock, &mattr);

    if(start_i2c() == false) {
      fprintf(stderr, "CANNOT START I2C TASK\n");
      return 1;
    }
    sleep(2);
    return 0;
}
