//
// Created by patlas on 4/25/19.
//

#ifndef _NRF_ASSERT_H_
#define _NRF_ASSERT_H_

#include <assert.h>

#define ASSERT(x) assert(x)

#endif //_NRF_ASSERT_H_
