/***************************************************************
 *      Copyright       SLabs 2016
 *      Project:        nrf52-core
 *      Creation date:  19/06/17
 *      Documentation:  https://s-labs.atlassian.net/wiki/spaces/TP/pages/121339960/I2C+module
 *
 *      2C communication manager header file.
 *****************************************************************/

#ifndef I2CCM_H
#define I2CCM_H

#include <stdint.h>
#include <stdbool.h>
#include <string.h>


/** @brief  Enum with events */
typedef enum
{
    I2CM_EVT_TX_SUCCESS = 0,   /* 0 */
    I2CM_EVT_TX_ADR_NACK,      /* 1 */
    I2CM_EVT_TX_DATA_NACK,     /* 2 */
    I2CM_EVT_RX_SUCCESS,       /* 3 */
    I2CM_EVT_RX_REQ_SUCCESS,   /* 4 */
    I2CM_EVT_RX_ADR_NACK,      /* 5 */
    I2CM_EVT_RX_DATA_NACK      /* 6 */
}m_i2ccm_evt_t;

/** @brief  Enum with module states */
typedef enum
{
    I2CM_STATE_NOT_INITIALIZED,               /* 0 */
    I2CM_STATE_IDLE,                          /* 1 */
    I2CM_STATE_TX_WAITING_FOR_CONFIRM,        /* 2 */
    I2CM_STATE_RX_REQ_WAITING_FOR_CONFIRM,    /* 3 */
    I2CM_STATE_WAITING_FOR_RX,                /* 4 */
    I2CM_STATE_RX_WAITING_FOR_CONFIRM         /* 5 */
}m_i2ccm_state_t;

/** @brief  Enum with error return states */
typedef enum
{
    I2CM_SUCCESS,               /* 0 */
    I2CM_ERROR_INVALID_STATE,   /* 1 */
    I2CM_ERROR_BUSY,            /* 2 */
    I2CM_ERROR_HW_INTERNAL,     /* 3 */
    I2CM_ERROR_INVALID_ADR,     /* 4 */
    I2CM_ERROR_DRV_TWI_ANACK,   /* 5 */
    I2CM_ERROR_DRV_TWI_DNACK,   /* 6 */
    I2CM_ERROR_DRV_TWI_OVERRUN, /* 7 */
    I2CM_ERROR_UNKOWN,          /* 8 */
    I2CM_ERROR_INVALID_CONF     /* 9 */
}m_i2ccm_error_t;

/** @brief  Structure for transmit data */
typedef struct
{
    uint8_t     device_address;                 /*!< Device address */
    uint8_t*    p_register;                     /*!< Pointer to register address */
    uint8_t     register_size;                  /*!< Register address size */
    uint8_t*    p_data;                         /*!< Pointer to data to send */
    uint8_t     data_size;                      /*!< Size of data to send */
    bool        no_stop;                        /*!< Without stop */
    void        (*evt_handler)(m_i2ccm_evt_t);  /*!< Event handler defined by caller module */
}m_i2ccm_tx_data_t;

/** @brief  Structure for receive request data */
typedef struct
{
    uint8_t     device_address;                 /*!< Device address */
    uint8_t*    p_register;                     /*!< Pointer to register address */
    uint8_t     register_size;                  /*!< Register address size */
    bool        no_stop;                        /*!< Without stop */
    void        (*evt_handler)(m_i2ccm_evt_t);  /*!< Event handler defined by caller module */
}m_i2ccm_rx_req_data_t;


/** @brief  Structure for receive data */
typedef struct
{
    uint8_t     device_address;    /*!< Device address */
    uint8_t*    p_data;            /*!< Pointer to data to receive */
    uint8_t     data_size;         /*!< Size of data to receive */
}m_i2ccm_rx_data_t;


#endif /* I2CCM_H */
