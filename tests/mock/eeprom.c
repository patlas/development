/***************************************************************
 *       Copyright       SLabs 2016
 *       Project:        nrf52-sensor-app
 *       Creation date:  29/06/17
 *       Documentation:  https://s-labs.atlassian.net/wiki/spaces/TP/pages/125796422/eeprom+module
 *       eeprom.
 *****************************************************************/

#include "eeprom.h"

#include "nrf_log.h"

#include <pthread.h>
#include <stdlib.h>
#include <time.h>

extern uint8_t eeprom_memory_map[131072];


uint8_t wrong_data[9] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

uint8_t written_data[256] = {0};
uint16_t written_size = 0;

uint8_t read_data[9] = {0};
uint8_t read_size = 0;

// corrupted, full, ok
uint8_t page_info[3][6] = {{0xFE,0,0,0,0,0}, {0x00,0xAA,0,0,0,0}, {0xFF,0xFF,0xFF,0xFF, 0xFF, 0xFF} };
const uint8_t strings[3][15] = {"CORRUPTED", "FULL", "READY TO WRITE"};

uint8_t *get_random_verify_read() {
    srand(time(NULL));
    uint8_t i = 0;//(uint8_t) (rand()%2);
    NRF_LOG_INFO("Choose random verify data: %s data readout\n", i?"DIFFERENT":"PROPER");
    if(i) {
        return wrong_data;
    }
    else {
        return written_data;
    }
}

uint8_t *get_random_page_option() {
    srand(time(NULL));
    uint8_t i = 2;//(uint8_t) (rand()%3);
    NRF_LOG_INFO("Choose random page option: %s\n", strings[i]);
    return page_info[i];
}

uint8_t *requested_data_ptr = NULL;

extern pthread_mutex_t rx_lock, tx_lock;
extern m_i2ccm_evt_t rx_event, tx_event;
extern uint8_t event_rx_count, event_tx_count;

static uint8_t m_eeprom_register_to_transmit[EEPROM_REG_SIZE] = {0};

m_i2ccm_tx_data_t m_eeprom_tx_data;
m_i2ccm_rx_data_t m_eeprom_rx_data;
m_i2ccm_rx_req_data_t m_eeprom_rx_req_data;


m_i2ccm_error_t eeprom_nonblocking_tx(uint32_t register_address, uint8_t const * data_to_write, uint16_t size,
        i2c_evt_handler hndl) {

    pthread_mutex_lock(&tx_lock);

    if(event_tx_count > 1)
    {
        pthread_mutex_unlock(&tx_lock);
        return I2CM_ERROR_BUSY;
    }

    written_size = size;
    memcpy(&eeprom_memory_map[register_address], data_to_write, size);

    NRF_LOG_DEBUG("eeprom_tx addr %d, size %d, data:", register_address, size);
    for(int i=0; i<size; i++)
        printf("0x%X, ", data_to_write[i]);
    printf("\n");


    tx_event = I2CM_EVT_TX_SUCCESS;
    NRF_LOG_DEBUG("Incrementing tx_evt_cnt\n");
    event_tx_count++;

    pthread_mutex_unlock(&tx_lock);

    return I2CM_SUCCESS;
}


m_i2ccm_error_t eeprom_nonblocking_rx(uint32_t register_address,  i2c_evt_handler hndl) {

    NRF_LOG_DEBUG("eeprom_rx addr %d\n", register_address);
//    m_i2ccm_error_t res = i2ccm_rx_request(m_eeprom_rx_req_data);

    while(0 != pthread_mutex_lock(&rx_lock));
    rx_event = I2CM_EVT_RX_REQ_SUCCESS;
    NRF_LOG_DEBUG("Incrementing rx_evt_cnt\n");
    event_rx_count++;
//    if(register_address % 256 == 252) { // PAGE_FLAGS_OFFSET
//        NRF_LOG_DEBUG("request read page info\n");
//        requested_data_ptr = get_random_page_option();
//    }
//    else {
//        requested_data_ptr = get_random_verify_read();
//    }
    requested_data_ptr = &eeprom_memory_map[register_address];
    pthread_mutex_unlock(&rx_lock);


    return I2CM_SUCCESS;
}

m_i2ccm_error_t eeprom_get_data(uint8_t *out_data, uint8_t size) {

//    pthread_mutex_lock(&rx_lock);
    rx_event = I2CM_EVT_RX_SUCCESS;
    NRF_LOG_DEBUG("Incrementing rx_evt_cnt\n");
    event_rx_count++;
//    out_data = requested_data_ptr;
    memcpy(out_data, requested_data_ptr, size);
//    pthread_mutex_unlock(&rx_lock);

    return I2CM_SUCCESS;
}

bool eeprom_blocking_rx(uint32_t register_address, uint8_t * data_to_read, uint16_t data_size) {
    // this function is only used by find index at startup so always read out only first page of eeprom
    memcpy(data_to_read, &eeprom_memory_map[register_address], data_size);
    return true;
}