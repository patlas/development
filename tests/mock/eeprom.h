#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define     EEPROM_BASE_ADR         0x50
#define     EEPROM_REG_SIZE         2

#include "i2ccm.h"

typedef void (*i2c_evt_handler)(m_i2ccm_evt_t param);

bool eeprom_blocking_tx(uint32_t register_address, uint8_t const * data_to_write,
                                   uint8_t data_size);
m_i2ccm_error_t eeprom_nonblocking_tx(uint32_t register_address, uint8_t const * data_to_write, uint16_t size,
                                             i2c_evt_handler hndl);

bool eeprom_blocking_rx(uint32_t register_address, uint8_t * data_to_read, uint16_t data_size);
m_i2ccm_error_t eeprom_nonblocking_rx(uint32_t register_address,  i2c_evt_handler hndl);
m_i2ccm_error_t eeprom_get_data(uint8_t *out_data, uint8_t size);
