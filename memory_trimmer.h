//
// Created by patlas on 4/1/19.
//

#ifndef NRF52_SENSOR_APPLICATION_MEMORY_TRIMMER_H
#define NRF52_SENSOR_APPLICATION_MEMORY_TRIMMER_H

#define MAX_RECORD_SIZE         9
#define MAX_PAGE_WRITE_TRIES    10                      /** 10 tries gives 90B corrupted cells in page -> at least 1/3 page
                                                         ** is corrupted so treat whole as corrupted */

#include <stdint.h>

#include "tests/mock/eeprom.h"

typedef enum {
    ALGORITHM_DISABLED = 1,
    BUS_MANAGER_ERROR,
    BUS_ACK_ERROR,
    CRITICAL_MEMORY_FAULT                            /** Cannot accumulate data in memory due to too much corrupted pages */
} trimm_error_t;

typedef enum
{
    WRITE_WITH_VERIFY = 0,
    WRITE_WITHOUT_VERIFY,
} trim_write_option_t;


typedef enum {
    TRIM_NONE = 0,
    TRIM_IDLE,
    TRIM_WRITE_RECORD_WITH_VERIFY,
    TRIM_VERIFY_WRITTEN_RECORD,
    TRIM_PREPARE_NEW_RECORD,
    TRIM_UPDATE_ENTRY_RECORD,
    TRIM_CLEAN_SAFETY_ENTRY_RECORD,
    TRIM_READ_PAGE_FLAGS,
    TRIM_VERIFY_PAGE_FLAGS,
} trim_state_t;

typedef struct
{
    uint16_t index;
    uint8_t offset;
    uint8_t write_tries;
    bool corrupted;
} trim_page_info_t;

typedef struct
{
    uint32_t addr;
    uint8_t data[MAX_RECORD_SIZE];
    uint8_t size;
} trim_memory_verify_t;


typedef void (*write_record_done_handler_t)(void *context);
typedef void (*error_handler_t)(void *context, const trimm_error_t er);

void memory_trimmer_state_machine(void);
void memory_trimmer_init(write_record_done_handler_t wr_hndl, void *wr_cnxt, error_handler_t er_hndl, void *er_cntx);
void memory_trimmer_get_last_record(uint8_t readout_buffer[]);
void memory_trimmer_write_record(const uint8_t *const data);

void memory_trimmer_rx_tx_handler(m_i2ccm_evt_t event);


// if def for test purposes
trim_page_info_t *get_m_page_info_ptr(void);



#endif //NRF52_SENSOR_APPLICATION_MEMORY_TRIMMER_H
