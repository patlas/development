#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <time.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <termios.h>
#include <string.h>
#include <sys/types.h>
#include <pthread.h>
#include <errno.h>

#include "functional_tests.h"
#include "i2c_simulator.h"

#include "memory_trimmer.h"




static pthread_t main_thread_hdl;



static void *main_thread(void *args)
{
    while(1)
    {
        memory_trimmer_state_machine();
    }
}

int main(void)
{
    int er = 0;
    er = pthread_create(&main_thread_hdl, NULL, main_thread, NULL);

    if(er!= 0)
    {
        return false;
    }

//    test_write_one_record();
//    test_write_whole_page();
//    test_write_whole_memory();
//    test_mark_page_as_corrupted();
//    test_omit_corrupted_and_write();
    test_load_latest_record_at_bootup_beginning();
    return 0;
}
