//
// Created by patlas on 4/1/19.
//

#include "memory_trimmer.h"
#include "eeprom.h"
#include "i2ccm.h"

#if defined(EEPROM_TRIM_MODULE_LOGS_LEVEL)
    #define NRF_LOG_LEVEL EEPROM_TRIM_MODULE_LOGS_LEVEL
#endif

#define NRF_LOG_MODULE_NAME "EEPROM_TRIM"
#include "nrf_log.h"
#include "nrf_assert.h"


#include <stdbool.h>


#define I2C_TRIES_LIMIT     10

#define EEPROM_PAGE_COUNT       512
#define EEPROM_PAGE_SIZE        256

#define MAX_PAGE_OFFSET         252                     /** nearest value to 256B (EEPROM_PAGE_SIZE) which
                                                         ** can be divided 9B (one measurement record in memory),
                                                         ** 6B (PAGE_FLAGS_SIZE) left are used for page corrupted
                                                         ** and/or clear flags */

#define MAX_PAGE_INDEX          EEPROM_PAGE_COUNT-1

#define PAGE_FLAGS_OFFSET       MAX_PAGE_OFFSET
#define PAGE_FLAGS_SIZE         6
#define RECORD_OFFSET_INC       MAX_RECORD_SIZE         /** data are stored in 24b tab for each of 3 measured phases */
#define FIRST_RECORD_PAGE       1

#define ENTRY_PAGE_ADDRESS      0
#define ENTRY_PAGE_OFFSET       2

#define MAX_PAGE_CORRUPTED      350                     /** Maximum count corrupted pages after writing will be disabled*/



#define PAGE_CORRUPTED          0xFE                   /** MAX value stored id 24 is 12.5MWs => 0xBEBC20 -> toggling one
                                                        ** bit in oldest part (0xBE) gives 0xFE */

#define CORRUPTED_FLAG_LAYOUT   0


// user callback functions pointers
static write_record_done_handler_t on_write_success_user_callback = NULL;
static error_handler_t on_error_user_callback = NULL;
void *on_done_context = NULL;
void *on_error_context = NULL;

// static function prototypes
inline static uint32_t calculate_memory_address(uint16_t page_number, uint8_t page_offset);
inline static void retry_write(void);
inline static bool check_memory_failure(void);
static bool verify_data_write(void);
static void write_memory(const uint32_t reg_addr, const uint8_t *const data, const uint16_t size, const trim_write_option_t wopt);
static void memory_read_request(uint32_t addr);
static void mark_page_as_corrupted(void);
static void load_latest_record(void);
static void update_page_entry(const uint16_t page_index);
static void readout_page_flags(void);
static void clear_page(const uint16_t page_index);
static void clean_entry_page(void);
static void clean_safety_entry(void);
static trim_state_t prepare_next_record_addr(void);


// memory trimmer buffers
static uint8_t m_eeprom_read_buffer[MAX_RECORD_SIZE] = {0};
static uint8_t m_eeprom_write_buffer[MAX_RECORD_SIZE] = {0};
static uint8_t m_eeprom_entry_buffer[ENTRY_PAGE_OFFSET] = {0};
static uint8_t m_eeprom_page_buffer[EEPROM_PAGE_SIZE] = {0};
static uint8_t m_latest_written_record[MAX_RECORD_SIZE] = {0};

// algorithm variables
static bool m_trim_enabled = false;
static bool m_retry_needed = false;
static bool m_record_ready_to_write = false;
static bool m_data_write_success = false;
static uint8_t m_size_to_read = 0;
static uint16_t m_entry_page_offset = 0;
static uint16_t m_corrupted_pages_count = 0;
static trim_state_t active_state = TRIM_NONE;
static trim_state_t pending_state = TRIM_NONE;
static trim_page_info_t m_page_info = {1, 0, 0, false};
static trim_memory_verify_t m_memory_to_verify = {0, {0}, MAX_RECORD_SIZE};


void memory_trimmer_rx_tx_handler(m_i2ccm_evt_t event)
{
    m_i2ccm_error_t status;

    NRF_LOG_ERROR("ACTIVE: %d, PENDING: %d\n", active_state, pending_state);

    switch(event)
    {
        case I2CM_EVT_TX_SUCCESS:
            active_state = pending_state;
            break;

        case I2CM_EVT_RX_REQ_SUCCESS:
            status = eeprom_get_data(m_eeprom_read_buffer, m_size_to_read);
            if(I2CM_SUCCESS == status)
            {
                NRF_LOG_INFO("eeprom_get_data -> SUCCESS\n");
            }
            else
            {
                NRF_LOG_ERROR("eeprom_get_data returned error. Returned status is: %u\r\n", status);
                on_error_user_callback(on_error_context, BUS_MANAGER_ERROR);
            }
            active_state = TRIM_IDLE;
            break;

        case I2CM_EVT_RX_SUCCESS:
        NRF_LOG_DEBUG("I2C state: I2CM_EVT_RX_SUCCESS, set pending=%d\n", pending_state);
            active_state = pending_state;
            break;

        case I2CM_EVT_TX_ADR_NACK:
        case I2CM_EVT_TX_DATA_NACK:
        case I2CM_EVT_RX_ADR_NACK:
        case I2CM_EVT_RX_DATA_NACK:
            // do some error
            ASSERT(false);
            break;

        default:
            //do some error
            ASSERT(false);
            break;
    }
}

void memory_trimmer_state_machine(void)
{
    if(m_trim_enabled)
    {
        switch(active_state)
        {
            case TRIM_NONE:
            case TRIM_IDLE:
                // do nothing
                break;

            case TRIM_WRITE_RECORD_WITH_VERIFY:
                NRF_LOG_DEBUG("Start verifying written record ...\n");
                pending_state = TRIM_VERIFY_WRITTEN_RECORD;
                active_state = TRIM_IDLE;
                m_size_to_read = m_memory_to_verify.size;
                memory_read_request(m_memory_to_verify.addr);
                break;

            case TRIM_VERIFY_WRITTEN_RECORD:
                if(!verify_data_write())
                {
                    NRF_LOG_INFO("Data write verification failed, getting next write try ...\n");
                    m_data_write_success = false;
                    m_retry_needed = true; // retry of write is needed in new address obtained below
                }
                else
                {
                    m_data_write_success = true;
                    NRF_LOG_INFO("Data write verification success\n");
                }
                m_record_ready_to_write = false;
                active_state = TRIM_PREPARE_NEW_RECORD;
                break;

            case TRIM_PREPARE_NEW_RECORD:
                active_state = TRIM_IDLE;
                if(MAX_PAGE_WRITE_TRIES <= m_page_info.write_tries && !m_page_info.corrupted)
                {
                    // exceeded limit of tries on this page => page corrupted, try on next page
                    NRF_LOG_DEBUG("Exceeded page write tries limit - page to mark as corrupted one\n");
                    m_page_info.corrupted = true;

                    pending_state = TRIM_PREPARE_NEW_RECORD;
                    mark_page_as_corrupted();
                    break;
                }

                if(TRIM_READ_PAGE_FLAGS != prepare_next_record_addr())
                {
                    // if new record created correctly just break in other case continue without break to page flags verification
                    NRF_LOG_DEBUG("New record ready to write\n");
                    m_record_ready_to_write = true;
                    if(m_data_write_success)
                    {
                        NRF_LOG_DEBUG("Data written correctly. New record ready for next data write.\n");
                        on_write_success_user_callback(on_done_context);
                    }
                }
                else
                {
                    // if above condition does not match continue TRIM_READ_PAGE_FLAGS case
                    active_state = TRIM_READ_PAGE_FLAGS;
                }
                break;

            case TRIM_READ_PAGE_FLAGS:
                m_page_info.corrupted = false;

                NRF_LOG_DEBUG("State: TRIM_READ_PAGE_FLAGS\n");
                pending_state = TRIM_VERIFY_PAGE_FLAGS;
                active_state = TRIM_IDLE;
                readout_page_flags();
                break;

            case TRIM_VERIFY_PAGE_FLAGS:
                NRF_LOG_DEBUG("State: TRIM_VERIFY_PAGE_FLAGS\n");
                if(PAGE_CORRUPTED == m_eeprom_read_buffer[CORRUPTED_FLAG_LAYOUT])
                {
                    NRF_LOG_DEBUG("Readout page flag value: CORRUPTED\n");
                    m_corrupted_pages_count++;
                    m_page_info.corrupted = true;
                    // TODO - remove above comment?
                    // still do not have new record index for future write operation because obtained page is corrupted
                    // goto must be called to force trigger next state in sate machine without any write/read i2c event occurance
                    active_state = TRIM_PREPARE_NEW_RECORD;
                }
                else
                {
                    m_page_info.corrupted = false;
                    // clear new data page and update entry page record
                    NRF_LOG_DEBUG("Clearing page ...\n");
                    m_record_ready_to_write = true; // TODO - connected with above
                    pending_state = TRIM_UPDATE_ENTRY_RECORD;
                    active_state = TRIM_IDLE;
                    clear_page(m_page_info.index);
                }

                break;

            case TRIM_UPDATE_ENTRY_RECORD:
                NRF_LOG_DEBUG("State: UPDATE_ENTRY_RECORD\n");
                active_state = TRIM_IDLE;

                if(EEPROM_PAGE_SIZE <= m_entry_page_offset)
                {
                    NRF_LOG_DEBUG("Clearing entry page ...\n");
                    m_entry_page_offset = 0;
                    pending_state = TRIM_UPDATE_ENTRY_RECORD;
                    clean_entry_page();
                    // if cleaning required just break after requested for cleaning
                    break;
                }

                // if start updating record from the beginning clean safety record at last available index
                if(0 == m_entry_page_offset)
                {
                    pending_state = TRIM_CLEAN_SAFETY_ENTRY_RECORD;
                }
                else
                {
                    pending_state = TRIM_PREPARE_NEW_RECORD;
                }

                update_page_entry(m_page_info.index);
                m_entry_page_offset += ENTRY_PAGE_OFFSET;
                break;

            case TRIM_CLEAN_SAFETY_ENTRY_RECORD:
                NRF_LOG_DEBUG("State: CLEAN_SAFETY_ENTRY_RECORD\n");
                pending_state = TRIM_PREPARE_NEW_RECORD;
                active_state = TRIM_IDLE;
                clean_safety_entry();
                break;

            default:
                //do some error
                ASSERT(false);
                break;
        }

        if(m_retry_needed && m_record_ready_to_write)
        {
            m_page_info.write_tries++;
            m_retry_needed = false;
            retry_write();
        }
    }
}



inline static uint32_t calculate_memory_address(uint16_t const page_number, uint8_t const page_offset)
{
    ASSERT(MAX_PAGE_INDEX >= page_number);
    return (uint32_t) (page_number * EEPROM_PAGE_SIZE + page_offset);
}

inline static void retry_write(void)
{
    NRF_LOG_DEBUG("Retrying to write record\n");
    memory_trimmer_write_record(NULL);
}

inline static bool check_memory_failure(void)
{
    if(MAX_PAGE_CORRUPTED <= m_corrupted_pages_count)
    {
        return true;
    }
    return false;
}


static void mark_page_as_corrupted(void)
{
    uint8_t data;
    uint32_t reg_addr;

    NRF_LOG_DEBUG("Marking page %d as corrupted\n", m_page_info.index);

    reg_addr = calculate_memory_address(m_page_info.index, PAGE_FLAGS_OFFSET + CORRUPTED_FLAG_LAYOUT);
    data = PAGE_CORRUPTED;
    write_memory(reg_addr, &data, 1, WRITE_WITHOUT_VERIFY);
}



static void write_memory(const uint32_t reg_addr, const uint8_t *const data, const uint16_t size, const trim_write_option_t wopt)
{
    ASSERT(data);

    m_i2ccm_error_t error;
    uint8_t tries = 0;

    if(WRITE_WITH_VERIFY == wopt)
    {

        pending_state = TRIM_WRITE_RECORD_WITH_VERIFY; //TODO - uncomment
        // fill memory verification data
        memcpy(m_memory_to_verify.data, data, MAX_RECORD_SIZE);
        m_memory_to_verify.addr = reg_addr;
    }

// TODO - consider to remove tries and return on error function only and clean all flags to default values and let user to deal with it

    do {
        tries++;
        error = eeprom_nonblocking_tx(reg_addr, data, size, memory_trimmer_rx_tx_handler);
        NRF_LOG_ERROR("TRY nr: %d\n", tries);
    } while(I2CM_SUCCESS != error && I2C_TRIES_LIMIT > tries);

    if(I2CM_SUCCESS != error)
    {
        on_error_user_callback(on_error_context, BUS_MANAGER_ERROR);
    }

}

static void memory_read_request(const uint32_t addr)
{
    m_i2ccm_error_t error;
    uint8_t tries = 0;

    do {
        tries++;
        error = eeprom_nonblocking_rx(addr, memory_trimmer_rx_tx_handler);
    } while(I2CM_SUCCESS != error && I2C_TRIES_LIMIT > tries);

    if(I2CM_SUCCESS != error)
    {
        on_error_user_callback(on_error_context, BUS_MANAGER_ERROR);
    }
}

static bool verify_data_write(void)
{
    return (0 == memcmp(m_memory_to_verify.data, m_eeprom_read_buffer, m_memory_to_verify.size));
}


static void load_latest_record(void)
{
    // find in page_entry_index page and set page_entry_index variable
    bool success;
    uint8_t rx_tries = 0;
    do {
        NRF_LOG_DEBUG("Read eeprom entry page\n");
        success = eeprom_blocking_rx(ENTRY_PAGE_ADDRESS, m_eeprom_page_buffer, (uint16_t) EEPROM_PAGE_SIZE);
        rx_tries++;
    } while(!success && I2C_TRIES_LIMIT > rx_tries);


    if(success)
    {
        // set page offset to latest possibly written data (last two bytes of entry page)
        m_entry_page_offset = EEPROM_PAGE_SIZE - ENTRY_PAGE_OFFSET;
        // start verification from second possible offset (if first one is empty than latest data is stored in last record which is assured by assignment above
        for(uint8_t i=0; i<(EEPROM_PAGE_SIZE/ENTRY_PAGE_OFFSET); i+=ENTRY_PAGE_OFFSET)
        {
            // max page index is 0x1FF (511) so if entry starts with 0xFFxx it means that this one has not been used yet
            if(0xFF == m_eeprom_page_buffer[i])
            {
                // if first is empty that means that latest one was in use before device restart
                if(0 != i)
                {
                    m_entry_page_offset = i - (uint8_t)ENTRY_PAGE_OFFSET;
                }
                break;
            }
        }

        NRF_LOG_DEBUG("Entry page offset: %d\n", m_entry_page_offset);
        // now page with latest written record is known, load this page and find latest entry
        m_page_info.index = (uint16_t) ((m_eeprom_page_buffer[m_entry_page_offset]<<8) & 0xFF00);
        m_page_info.index |= m_eeprom_page_buffer[m_entry_page_offset+1];
        m_page_info.offset = 0;

        m_record_ready_to_write = false;
        // if offset is larger than MAX than it means that memory contains wrong data (e.g. whole memory empty at startup) then set index to the first record page
        if(MAX_PAGE_INDEX < m_page_info.index)
        {
            m_page_info.index = FIRST_RECORD_PAGE;
            m_record_ready_to_write = true;
        }
        rx_tries = 0;
        uint32_t addr = calculate_memory_address(m_page_info.index, 0);

        do {
            NRF_LOG_DEBUG("Reading eeprom page with latest written record\n");
            success = eeprom_blocking_rx(addr, m_eeprom_page_buffer, (uint16_t) EEPROM_PAGE_SIZE);
            rx_tries++;
        } while(!success && I2C_TRIES_LIMIT > rx_tries);

        if(success)
        {
            // iterate over records and find latest one
            for(uint8_t offset=0; offset<MAX_PAGE_OFFSET; offset+=RECORD_OFFSET_INC)
            {
                // if first byte of record starts with 0xFF it means that record is empty and ready to write so previous one is the one looked for
                if(0xFF == m_eeprom_page_buffer[offset])
                {
                    // set current page offset to found one
                    m_page_info.offset = offset;
                    break;
                }
            }
            // set latest written record value
            memcpy(m_latest_written_record, &m_eeprom_page_buffer[m_page_info.offset], MAX_RECORD_SIZE);

            // call prepare next record and algorithm is ready to use
            m_page_info.write_tries = 0;
            m_page_info.corrupted = false;

            NRF_LOG_DEBUG("Latest record page %d has offset %d\n", m_page_info.index, m_page_info.offset);

            if(m_record_ready_to_write)
            {
                // no index entry in entry page so update it
                pending_state = TRIM_IDLE;
                update_page_entry(m_page_info.index);
                while(active_state != TRIM_IDLE);
                m_entry_page_offset += ENTRY_PAGE_OFFSET;
            }
        }
        else
        {
            on_error_user_callback(on_error_context, BUS_MANAGER_ERROR);
        }
    }
    else
    {
        on_error_user_callback(on_error_context, BUS_MANAGER_ERROR);
    }
}

static void readout_page_flags(void)
{
    uint32_t addr = calculate_memory_address(m_page_info.index, PAGE_FLAGS_OFFSET);
    m_size_to_read = PAGE_FLAGS_SIZE;
    NRF_LOG_DEBUG("Send page %d read flag request\n", m_page_info.index);
    memory_read_request(addr);
}

static void clear_page(const uint16_t page_index)
{
    // cannot erase first page because it contains information about whole memory usage and current data index
    ASSERT(0 != page_index);

    uint8_t clear_buff[EEPROM_PAGE_SIZE];
    memset(clear_buff, 0xFF, EEPROM_PAGE_SIZE);
    uint32_t addr = calculate_memory_address(page_index, 0);
    write_memory(addr, clear_buff, EEPROM_PAGE_SIZE, WRITE_WITHOUT_VERIFY);
}


static void clean_entry_page(void)
{
    // page full, clean it up
    uint8_t clear_buff[EEPROM_PAGE_SIZE];
    memset(clear_buff, 0xFF, EEPROM_PAGE_SIZE);
    // clear almost all page entry except latest record -> in case of memory loss it will still contain info about latest write
    write_memory(ENTRY_PAGE_ADDRESS, clear_buff, EEPROM_PAGE_SIZE - ENTRY_PAGE_OFFSET, WRITE_WITHOUT_VERIFY);
}

static void update_page_entry(const uint16_t page_index)
{
    // prepare new index for next entry write
    m_eeprom_entry_buffer[0] = (uint8_t) (page_index >> 8);
    m_eeprom_entry_buffer[1] = (uint8_t) page_index;

    NRF_LOG_DEBUG("Writting new record into entry page at offset: %d\n", m_entry_page_offset);

    write_memory(m_entry_page_offset, m_eeprom_entry_buffer, ENTRY_PAGE_OFFSET, WRITE_WITHOUT_VERIFY);
}

static void clean_safety_entry(void)
{
    // prepare new index for next entry write
    m_eeprom_entry_buffer[0] = 0xFF;
    m_eeprom_entry_buffer[1] = 0xFF;

    NRF_LOG_DEBUG("Cleaning safety record in entry page\n");

    write_memory((EEPROM_PAGE_SIZE - ENTRY_PAGE_OFFSET), m_eeprom_entry_buffer, ENTRY_PAGE_OFFSET, WRITE_WITHOUT_VERIFY);
}


static trim_state_t prepare_next_record_addr(void)
{
    NRF_LOG_DEBUG("Preparing new write record\n");

    // check if memory is too corrupted for being used
    if(check_memory_failure())
    {
        // critical memory failure - disable algorithm
        m_trim_enabled = false;
        NRF_LOG_INFO("Disabling trimming algorithm due to memory critical fault\n");
        on_error_user_callback(on_error_context, ALGORITHM_DISABLED);
        return TRIM_IDLE;
    }

    if(!m_record_ready_to_write)
    {
        NRF_LOG_DEBUG("Incrementing page offset\n");
        m_page_info.offset += RECORD_OFFSET_INC;
    }

    if(m_page_info.corrupted || MAX_PAGE_OFFSET <= m_page_info.offset)
    {
        // if on latest page of memory then set index to first one
        if(MAX_PAGE_INDEX < ++m_page_info.index)
        {
            NRF_LOG_DEBUG("Page index reach tail starting from head\n");
            m_page_info.index = FIRST_RECORD_PAGE;
            m_corrupted_pages_count = 0;
        }
        m_page_info.write_tries = 0;
        m_page_info.offset = 0;

        // send request to read memory page status flags -> in response asynchronous response react if corrupted or full
        NRF_LOG_DEBUG("Getting new page, and read out its page info flags\n");

        return TRIM_READ_PAGE_FLAGS;
    }
    return TRIM_IDLE;
}

void memory_trimmer_init(write_record_done_handler_t wr_hndl, void *wr_cnxt, error_handler_t er_hndl, void *er_cntx)
{
    // error function must be set
    ASSERT(NULL != er_hndl);

    // set user on write done and on error handler functions and its contexts
    on_write_success_user_callback = wr_hndl;
    on_error_user_callback = er_hndl;
    on_done_context = wr_cnxt;
    on_error_context = er_cntx;

    m_trim_enabled = true;

    // find first empty record to continue storing data in memory
    load_latest_record();
}

void memory_trimmer_get_last_record(uint8_t readout_buffer[MAX_RECORD_SIZE])
{
    memcpy(readout_buffer, m_latest_written_record, MAX_RECORD_SIZE);
}

void memory_trimmer_write_record(const uint8_t *const data)
{
    // if trim algorithm enabled than allow user to write data otherwise do nothing
    if(m_trim_enabled)
    {
        // if no new data ready to write, retry write old one
        if (NULL != data)
        {
            memcpy(m_eeprom_write_buffer, data, MAX_RECORD_SIZE);
        }
        uint32_t reg_addr = calculate_memory_address(m_page_info.index, m_page_info.offset);
        write_memory(reg_addr, m_eeprom_write_buffer, MAX_RECORD_SIZE, WRITE_WITH_VERIFY);
    }
    else
    {
        NRF_LOG_INFO("Trimming algorithm disabled -> possibly due to memory critical fault\n");
        on_error_user_callback(on_error_context, ALGORITHM_DISABLED);
    }
}



// ifdef for test purposes
trim_page_info_t *get_m_page_info_ptr(void)
{
    return &m_page_info;
}